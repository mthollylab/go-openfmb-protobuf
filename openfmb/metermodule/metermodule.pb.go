// Code generated by protoc-gen-go. DO NOT EDIT.
// source: metermodule/metermodule.proto

package metermodule

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "gitlab.com/mthollylab/go-openfmb-protobuf/openfmb"
	commonmodule "gitlab.com/mthollylab/go-openfmb-protobuf/openfmb/commonmodule"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// Resource reading value
type MeterReading struct {
	// UML inherited base object
	ConductingEquipmentTerminalReading *commonmodule.ConductingEquipmentTerminalReading `protobuf:"bytes,1,opt,name=conductingEquipmentTerminalReading,proto3" json:"conductingEquipmentTerminalReading,omitempty"`
	// MISSING DOCUMENTATION!!!
	PhaseMMTN *commonmodule.PhaseMMTN `protobuf:"bytes,2,opt,name=phaseMMTN,proto3" json:"phaseMMTN,omitempty"`
	// MISSING DOCUMENTATION!!!
	ReadingMMTR *commonmodule.ReadingMMTR `protobuf:"bytes,3,opt,name=readingMMTR,proto3" json:"readingMMTR,omitempty"`
	// MISSING DOCUMENTATION!!!
	ReadingMMXU          *commonmodule.ReadingMMXU `protobuf:"bytes,4,opt,name=readingMMXU,proto3" json:"readingMMXU,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                  `json:"-"`
	XXX_unrecognized     []byte                    `json:"-"`
	XXX_sizecache        int32                     `json:"-"`
}

func (m *MeterReading) Reset()         { *m = MeterReading{} }
func (m *MeterReading) String() string { return proto.CompactTextString(m) }
func (*MeterReading) ProtoMessage()    {}
func (*MeterReading) Descriptor() ([]byte, []int) {
	return fileDescriptor_3a3208d1cde56c6c, []int{0}
}

func (m *MeterReading) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_MeterReading.Unmarshal(m, b)
}
func (m *MeterReading) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_MeterReading.Marshal(b, m, deterministic)
}
func (m *MeterReading) XXX_Merge(src proto.Message) {
	xxx_messageInfo_MeterReading.Merge(m, src)
}
func (m *MeterReading) XXX_Size() int {
	return xxx_messageInfo_MeterReading.Size(m)
}
func (m *MeterReading) XXX_DiscardUnknown() {
	xxx_messageInfo_MeterReading.DiscardUnknown(m)
}

var xxx_messageInfo_MeterReading proto.InternalMessageInfo

func (m *MeterReading) GetConductingEquipmentTerminalReading() *commonmodule.ConductingEquipmentTerminalReading {
	if m != nil {
		return m.ConductingEquipmentTerminalReading
	}
	return nil
}

func (m *MeterReading) GetPhaseMMTN() *commonmodule.PhaseMMTN {
	if m != nil {
		return m.PhaseMMTN
	}
	return nil
}

func (m *MeterReading) GetReadingMMTR() *commonmodule.ReadingMMTR {
	if m != nil {
		return m.ReadingMMTR
	}
	return nil
}

func (m *MeterReading) GetReadingMMXU() *commonmodule.ReadingMMXU {
	if m != nil {
		return m.ReadingMMXU
	}
	return nil
}

// Resource reading profile
type MeterReadingProfile struct {
	// UML inherited base object
	ReadingMessageInfo *commonmodule.ReadingMessageInfo `protobuf:"bytes,1,opt,name=readingMessageInfo,proto3" json:"readingMessageInfo,omitempty"`
	// MISSING DOCUMENTATION!!!
	Ied *commonmodule.IED `protobuf:"bytes,2,opt,name=ied,proto3" json:"ied,omitempty"`
	// MISSING DOCUMENTATION!!!
	Meter *commonmodule.Meter `protobuf:"bytes,3,opt,name=meter,proto3" json:"meter,omitempty"`
	// MISSING DOCUMENTATION!!!
	MeterReading         *MeterReading `protobuf:"bytes,4,opt,name=meterReading,proto3" json:"meterReading,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *MeterReadingProfile) Reset()         { *m = MeterReadingProfile{} }
func (m *MeterReadingProfile) String() string { return proto.CompactTextString(m) }
func (*MeterReadingProfile) ProtoMessage()    {}
func (*MeterReadingProfile) Descriptor() ([]byte, []int) {
	return fileDescriptor_3a3208d1cde56c6c, []int{1}
}

func (m *MeterReadingProfile) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_MeterReadingProfile.Unmarshal(m, b)
}
func (m *MeterReadingProfile) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_MeterReadingProfile.Marshal(b, m, deterministic)
}
func (m *MeterReadingProfile) XXX_Merge(src proto.Message) {
	xxx_messageInfo_MeterReadingProfile.Merge(m, src)
}
func (m *MeterReadingProfile) XXX_Size() int {
	return xxx_messageInfo_MeterReadingProfile.Size(m)
}
func (m *MeterReadingProfile) XXX_DiscardUnknown() {
	xxx_messageInfo_MeterReadingProfile.DiscardUnknown(m)
}

var xxx_messageInfo_MeterReadingProfile proto.InternalMessageInfo

func (m *MeterReadingProfile) GetReadingMessageInfo() *commonmodule.ReadingMessageInfo {
	if m != nil {
		return m.ReadingMessageInfo
	}
	return nil
}

func (m *MeterReadingProfile) GetIed() *commonmodule.IED {
	if m != nil {
		return m.Ied
	}
	return nil
}

func (m *MeterReadingProfile) GetMeter() *commonmodule.Meter {
	if m != nil {
		return m.Meter
	}
	return nil
}

func (m *MeterReadingProfile) GetMeterReading() *MeterReading {
	if m != nil {
		return m.MeterReading
	}
	return nil
}

func init() {
	proto.RegisterType((*MeterReading)(nil), "metermodule.MeterReading")
	proto.RegisterType((*MeterReadingProfile)(nil), "metermodule.MeterReadingProfile")
}

func init() { proto.RegisterFile("metermodule/metermodule.proto", fileDescriptor_3a3208d1cde56c6c) }

var fileDescriptor_3a3208d1cde56c6c = []byte{
	// 371 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x92, 0x3f, 0x4f, 0xf2, 0x40,
	0x1c, 0xc7, 0xd3, 0xc2, 0xf3, 0xe4, 0xe1, 0x60, 0x79, 0x8e, 0xc1, 0x4a, 0x62, 0x24, 0x4c, 0x2e,
	0x50, 0x23, 0x71, 0x32, 0x2e, 0x28, 0x31, 0x0c, 0x35, 0xe4, 0x02, 0x86, 0xb8, 0xb5, 0xf4, 0x5a,
	0x2e, 0xb9, 0x3f, 0xb5, 0xb4, 0x83, 0x9b, 0x93, 0x71, 0x74, 0xf6, 0x25, 0xb8, 0xf3, 0x1e, 0x7c,
	0x59, 0xa6, 0xed, 0x21, 0xbf, 0x4b, 0x88, 0xba, 0xc1, 0xf5, 0xf3, 0xf9, 0xfe, 0xee, 0xbe, 0xf9,
	0xa1, 0x23, 0x41, 0x33, 0x9a, 0x0a, 0x15, 0xe6, 0x9c, 0xba, 0xe0, 0xf7, 0x20, 0x49, 0x55, 0xa6,
	0x70, 0x13, 0x1c, 0x75, 0x1a, 0xb9, 0xe0, 0xd5, 0x79, 0xe7, 0x78, 0xa9, 0x84, 0x50, 0x52, 0x7b,
	0xf0, 0x4f, 0x05, 0xf4, 0x3e, 0x6c, 0xd4, 0xf2, 0x0a, 0x97, 0x50, 0x3f, 0x64, 0x32, 0xc6, 0xcf,
	0x16, 0xea, 0x2d, 0x95, 0x0c, 0xf3, 0x65, 0xc6, 0x64, 0x3c, 0x7e, 0xc8, 0x59, 0x22, 0xa8, 0xcc,
	0x66, 0x34, 0x15, 0x4c, 0xfa, 0x5c, 0x63, 0x8e, 0xd5, 0xb5, 0x4e, 0x9a, 0x67, 0xa7, 0x03, 0x23,
	0xf2, 0xea, 0x47, 0x6f, 0x54, 0x7f, 0xda, 0x38, 0x16, 0xf9, 0xc5, 0x04, 0x7c, 0x8e, 0x1a, 0xc9,
	0xca, 0x5f, 0x53, 0xcf, 0x9b, 0xdd, 0x3a, 0x76, 0x39, 0xee, 0xc0, 0x1c, 0x37, 0xdd, 0x7e, 0x26,
	0x3b, 0x12, 0x5f, 0xa0, 0x66, 0x5a, 0x25, 0x78, 0xde, 0x8c, 0x38, 0xb5, 0x52, 0x3c, 0x34, 0x45,
	0xb2, 0x03, 0x08, 0xa4, 0x0d, 0x79, 0x31, 0x77, 0xea, 0xdf, 0xca, 0x8b, 0x39, 0x81, 0x74, 0xef,
	0xcd, 0x46, 0x6d, 0x58, 0xe5, 0x34, 0x55, 0x11, 0xe3, 0x14, 0xdf, 0x21, 0xbc, 0xc5, 0xe8, 0x7a,
	0xed, 0xc7, 0x74, 0x22, 0x23, 0xa5, 0x0b, 0xec, 0xee, 0xcf, 0xde, 0x71, 0xba, 0xb0, 0x3d, 0x09,
	0xb8, 0x8f, 0x6a, 0x8c, 0x86, 0xba, 0x9a, 0xff, 0x66, 0xd0, 0x64, 0x7c, 0x3d, 0xfa, 0xf7, 0xb2,
	0x71, 0xac, 0xd7, 0xc2, 0x2e, 0x38, 0x3c, 0x44, 0x7f, 0xca, 0x25, 0xd1, 0x95, 0xb4, 0x4d, 0xa1,
	0xbc, 0x38, 0x50, 0x2a, 0x16, 0xdf, 0xa0, 0x96, 0x00, 0x4f, 0xfa, 0x6a, 0x04, 0x6e, 0x20, 0x7c,
	0x33, 0x48, 0x30, 0xc4, 0x11, 0x47, 0x6d, 0x95, 0x50, 0x19, 0x89, 0x00, 0xba, 0x53, 0xeb, 0xfe,
	0x32, 0x66, 0x19, 0xf7, 0x83, 0xe2, 0x36, 0xae, 0xc8, 0x56, 0x8a, 0xf3, 0x47, 0xee, 0x07, 0x6e,
	0xac, 0xfa, 0x9a, 0xef, 0x97, 0x8b, 0x1a, 0xe4, 0x91, 0xab, 0x0f, 0xe0, 0xfa, 0xbf, 0xdb, 0xfb,
	0x62, 0x83, 0xbf, 0xa5, 0x33, 0xfc, 0x0c, 0x00, 0x00, 0xff, 0xff, 0x85, 0xc9, 0xd9, 0xb9, 0x36,
	0x03, 0x00, 0x00,
}
